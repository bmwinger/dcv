#!/usr/bin/python

import os
import argparse
from curses import wrapper
import curses

parser = argparse.ArgumentParser(description='Command line interface to view differences between morrowind mods')
parser.add_argument("directories", metavar="DIR", help="Directories to compare", nargs='*')

args = parser.parse_args()

filelist = []

for directory in args.directories:
    files = set()
    for (dirpath, dirnames, filenames) in os.walk(directory):
        newfiles = set([os.path.relpath(os.path.join(dirpath, file), start=directory).lower() for file in filenames if not file.endswith('.txt')])
        files = files.union(newfiles)

    filelist.append({"dir":directory, "files":files})


def get_conflicts(directory):
    this_index = filelist.index(directory)

    conflicts = ""
    for (index, other_dir) in enumerate(filelist):
        other_dir_list = []
        if index != this_index:
            other_dir_list = other_dir['files'].intersection(directory['files'])

        if len(other_dir_list) > 0:
            if this_index > index:
                conflicts += "Higher - {}:\n\t".format(other_dir['dir']) + "\n\t".join(other_dir_list)
            else:
                conflicts += "Lower - {}:\n\t".format(other_dir['dir']) + "\n\t".join(other_dir_list)
            conflicts += '\n'

    return conflicts

def main(stdscr):
    leftwin = curses.newpad(curses.LINES, int(curses.COLS/2))
    rightwin = curses.newpad(curses.LINES, int(curses.COLS/2))
    curses.curs_set( 0 )
    rightwin.scrollok(True)
    leftwin.scrollok(True)

    cur_index = 0
    left = True
    refresh_right = True
    refresh_left = True
    right_pos = 0
    left_pos = 0
    max_row = curses.LINES

    while True:

        curses.flushinp()
        if refresh_left:
            leftwin.clear()
            for index in range(left_pos, min(len(filelist), left_pos + curses.LINES - 1)):
                directory = filelist[index]
                if index == cur_index:
                    leftwin.addstr(directory['dir'] + '\n', curses.A_STANDOUT)
                else:
                    leftwin.addstr(directory['dir'] + '\n')
            refresh_left = False
            right_pos = 0

        if refresh_right:
            rightwin.clear()
            right_val = get_conflicts(filelist[cur_index]).splitlines()
            for i in range(right_pos, min(len(right_val), right_pos + curses.LINES - 1)):
                rightwin.addstr(right_val[i] + '\n')
            refresh_right = False
            right_len = len(right_val)

        stdscr.refresh()
        leftwin.refresh(0, 0, 0, 0, curses.LINES, int(curses.COLS/2))
        rightwin.refresh(0, 0, 0, int(curses.COLS/2), curses.LINES, curses.COLS)

        c = stdscr.getch()
        if c == curses.KEY_UP:
            if left:
                cur_index = (cur_index + len(filelist) - 1) % len(filelist)
                refresh_left = True
                refresh_right = True
            else:
                if right_pos > 0:
                    right_pos -= 1
                refresh_right = True
        elif c == curses.KEY_DOWN:
            if left:
                cur_index = (cur_index + len(filelist) + 1) % len(filelist)
                refresh_left = True
                refresh_right = True
            else:
                if right_pos < right_len:
                    right_pos += 1
                refresh_right = True
        elif c == curses.KEY_NPAGE:
            if left:
                cur_index = (cur_index + int(curses.LINES/2)) % len(filelist)
                refresh_left = True
                refresh_right= True
        elif c == curses.KEY_PPAGE:
            if left:
                cur_index = (cur_index + len(filelist) - int(curses.LINES/2)) % len(filelist)
                refresh_left = True
                refresh_right = True
        elif c == curses.KEY_RIGHT or c == curses.KEY_LEFT:
            left = not left
        elif c == ord('q'):
            break  # Exit on q

        if cur_index > int(curses.LINES/2):
            left_pos = cur_index - int(curses.LINES/2)
        elif cur_index <= int(curses.LINES/2):
            left_pos = 0
        if cur_index - left_pos < -5:
            left_pos = cur_index + 5

wrapper(main)
