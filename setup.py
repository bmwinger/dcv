#!/usr/bin/env python

from distutils.core import setup

setup(name='Directory Conflict Viewer',
    version='1.0',
    url='https://gitlab.com/bmwinger/dcv',
    scripts=['dcv'],
 )
