# Directory Conflict Viewer

A script for quickly determining files that would conflict in a list of directories if those directories would be merged.

Intended as a way of determining file conflicts for mods for games such as OpenMW.
